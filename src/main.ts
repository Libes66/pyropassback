import { AppModule } from './app.module';
import { PrismaService } from './prisma.service';
import { NestFactory } from '@nestjs/core';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const prismaService = app.get(PrismaService);
  app.enableCors({
    origin: '*',
  });
  app.setGlobalPrefix(process.env.GLOBAL_PREFIX);
  await app.listen(4200);
}
bootstrap();
