import {ForbiddenException, Injectable} from '@nestjs/common';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import {PrismaService} from "../prisma.service";

@Injectable()
export class RoomsService {
  constructor(private prisma: PrismaService) {}

  // Создание комнаты с указанием владельца
  async createRoom(name: string, ownerId: number) {
    return this.prisma.room.create({
      data: {
        name,
        owner: { connect: { id: ownerId } },
      },
    });
  }

  // Получение комнаты с проверкой доступа
  async getRoomById(roomId: number, userId: number) {
    const room = await this.prisma.room.findUnique({
      where: { id: Number(roomId)},
      include: { owner: true, members: { include: { user: true } } },
    });
    if (!room) throw new Error('Комната не найдена');
    console.log(room.ownerId,userId);
    if (room.ownerId !== userId && !room.members.some(m => m.userId === userId)) {
      throw new ForbiddenException('Нет доступа к комнате');
    }
    return room;
  }

  // Добавление пользователя в комнату
  async addUserToRoom(roomId: number, ownerId: number, userId: number) {
    const room = await this.prisma.room.findUnique({
      where: { id: roomId },
    });
    if (!room) throw new Error('Комната не найдена');
    if (room.ownerId !== ownerId) {
      throw new ForbiddenException('Только владелец может добавлять участников');
    }

    return this.prisma.roomMember.create({
      data: {
        roomId,
        userId,
      },
    });
  }
}
