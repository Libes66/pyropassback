import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RoomsService } from './rooms.service';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';

@Controller('rooms')
export class RoomsController {
  constructor(private readonly roomsService: RoomsService) {}


  @Post('create')
  async createRoom(@Body('name') name: string, @Body('ownerId') ownerId: number) {
    return this.roomsService.createRoom(name, ownerId);
  }

  @Get(':roomId')
  async getRoom(@Param('roomId') roomId: number, @Body('userId') userId: number) {
    return this.roomsService.getRoomById(roomId, userId);
  }

  @Post(':roomId/add-user')
  async addUserToRoom(
      @Param('roomId') roomId: number,
      @Body('ownerId') ownerId: number,
      @Body('userId') userId: number,
  ) {
    return this.roomsService.addUserToRoom(roomId, ownerId, userId);
  }
}
