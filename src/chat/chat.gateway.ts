// chat.gateway.ts
import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { RoomsService } from '../rooms/rooms.service';
import {MessageService} from "../message/message.service";

@WebSocketGateway({  namespace: 'chat',
  cors: {
  origin: '*',
  } })
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  constructor(
      private readonly messagesService: MessageService,
      private readonly roomsService: RoomsService,
  ) {}

  // Подключение пользователя
  handleConnection(client: Socket) {
    console.log(`Client connected: ${client.id}`);
  }

  // Отключение пользователя
  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('joinRoom')
  async handleJoinRoom(
      @MessageBody('roomId') roomId: number,
      @MessageBody('userId') userId: number,
      @ConnectedSocket() client: Socket,
  ) {
    const room = await this.roomsService.getRoomById(Number(roomId), Number(userId));
    if (room) {
      client.join(`room-${roomId}`);
      client.emit('joinedRoom', { roomId });
      console.log(`room-${roomId}`);
    } else {
      client.emit('error', { message: 'No access to the room' });
    }
  }

  @SubscribeMessage('sendMessage')
  async handleMessage(
      @MessageBody('roomId') roomId: number,
      @MessageBody('userId') userId: number,
      @MessageBody('content') content: string,
      @ConnectedSocket() client: Socket,
  ) {
    const room = await this.roomsService.getRoomById(roomId, userId);
    if (!room) {
      client.emit('error', { message: 'No access to the room' });
      return;
    }

    const message = await this.messagesService.createMessage(Number(userId), Number(roomId), content);

    client.to(`room-${roomId}`).emit("message", { message: message});
  }
}
