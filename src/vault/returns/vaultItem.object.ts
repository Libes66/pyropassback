import {Prisma} from "@prisma/client";

export const returnVaultObject:Prisma.VaultSelect ={
    id: true,
    name: true,
    image: true,
    updatedAt:true,
    createdAt:true,
    code:true,
    description:true,
    vaultItems:{
        select:{
            id:true,
            code:true,
            name:true,
            description:true,
            type:true,
            data:true,
        }
    }
}
