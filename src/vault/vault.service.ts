import { Injectable } from '@nestjs/common';
import { CreateVaultDto } from './dto/create-vault.dto';
import { UpdateVaultDto } from './dto/update-vault.dto';
import {PrismaService} from "../prisma.service";
import {returnVaultObject} from "./returns/vaultItem.object";

@Injectable()
export class VaultService {
  constructor(private prisma: PrismaService) {}

  create(createVaultDto: CreateVaultDto) {
    return 'This action adds a new vault';
  }

  findAll() {
    return `This action returns all vault`;
  }

  findOne(id: number) {
    console.log(id);
   return this.prisma.vault.findUnique({
     where:{ id: id },
     select:{
      ...returnVaultObject
     }
   })
  }

  update(id: number, updateVaultDto: UpdateVaultDto) {
    return `This action updates a #${id} vault`;
  }

  remove(id: number) {
    return `This action removes a #${id} vault`;
  }

  // Получить все свои vault
  async getAllUserVaults(userId: number) {
    // Ищем все vault, созданные текущим пользователем
    return this.prisma.vault.findMany({
      where: {
        createdBy: userId, // Фильтр по полю createdBy
      },
      include: {
        vaultItems:true//
      },
    });
  }

  // Получить все расшаренные vault
  async getSharedVaults(userId: number) {
    // Ищем все vault, расшаренные текущему пользователю
    return this.prisma.vault.findMany({
      where: {
        vaultShares:{
        some:{
          userId:userId
        }
        }

      },
      include: {
        vaultItems: true, // Включаем связанные VaultItem
      },
    });
  }

  // Получить все vault (для администратора или других целей)
  async getAllVaults(userId: number) {
    return this.prisma.vault.findMany({
      where: {
        OR: [
          // Условие: записи, которые принадлежат пользователю
          { createdBy: userId },

          // Условие: записи, которые были расшарены с пользователем
          {
            vaultShares:{
              some:{
                userId:userId
              }
            }
          },
        ],
      },
      include: {
        vaultItems:true
      },
    });
  }
}
