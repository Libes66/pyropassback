import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { VaultService } from './vault.service';
import { CreateVaultDto } from './dto/create-vault.dto';
import { UpdateVaultDto } from './dto/update-vault.dto';
import {Auth} from "../auth/decorators/auth.decorator";
import {CurrentUser} from "../auth/decorators/user.decorator";

@Controller('vault')
export class VaultController {
  constructor(private readonly vaultService: VaultService) {}

  @Post()
  @Auth()
  create(@CurrentUser('id') id:number,@Body() createVaultDto: CreateVaultDto) {
    return this.vaultService.create(createVaultDto);
  }
  @Post('item')
  @Auth()
  createItem(@CurrentUser('id') id:number,@Body() createVaultItemDto: CreateVaultDto) {
    return ''
  }
  @Get(':type')
  @Auth()
  async getVaults(@CurrentUser('id') id:number, @Param('type') type: string) {
    const userId = id;

    switch (type) {
      case 'shared':
        return this.vaultService.getSharedVaults(userId); // Возвращаем все расшаренные vault
      case 'all':
        return this.vaultService.getAllVaults(userId); // Возвращаем все vault
      default:
        return this.vaultService.getAllUserVaults(userId); // Обработка неверного типа
    }
  }

  @Get('item/:id')
  @Auth()
  findOne(@Param('id') id: string) {

    return this.vaultService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVaultDto: UpdateVaultDto) {
    return this.vaultService.update(+id, updateVaultDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.vaultService.remove(+id);
  }
}
