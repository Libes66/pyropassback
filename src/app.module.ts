import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaService } from './prisma.service';
import { UserModule } from './user/user.module';

import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { S3Module } from './s3/s3.module';
import { ServiceModule } from './service/service.module';
import { VaultModule } from './vault/vault.module';
import { CategoryModule } from './category/category.module';
import { RoomsModule } from './rooms/rooms.module';
import { MessageModule } from './message/message.module';
import {ChatGateway} from "./chat/chat.gateway";


@Module({
  imports: [ConfigModule.forRoot(),AuthModule, UserModule,ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'uploads'),

  }), S3Module, ServiceModule, VaultModule, CategoryModule, RoomsModule, MessageModule],
  controllers: [AppController],
  providers: [AppService,PrismaService,ChatGateway],
})
export class AppModule {}

