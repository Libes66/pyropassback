import * as AWS from 'aws-sdk';

AWS.config.update({
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    region: process.env.S3_REGION,

});


const s3 = new AWS.S3({
    endpoint: process.env.S3_ENDPOINT, 
    s3ForcePathStyle: true,
});
export { s3 };
