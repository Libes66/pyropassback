import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import {PrismaService} from "../prisma.service";
import {RoomsService} from "../rooms/rooms.service";
import {ChatGateway} from "../chat/chat.gateway";

@Module({
  controllers: [MessageController],
  providers: [MessageService,PrismaService,RoomsService],
  exports:[MessageService]
})
export class MessageModule {}
