import {Controller, Get, Post, Body, Patch, Param, Delete, ForbiddenException} from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import {RoomsService} from "../rooms/rooms.service";

@Controller('message')
export class MessageController {
  constructor(
      private readonly messagesService: MessageService,
      private readonly roomsService: RoomsService,
  ) {}

  // Отправка сообщения в комнату
  @Post('send')
  async sendMessage(
      @Body('roomId') roomId: number,
      @Body('userId') userId: number,
      @Body('content') content: string,
  ) {
    const room = await this.roomsService.getRoomById(roomId, userId);
    if (!room) {
      throw new ForbiddenException('Нет доступа к комнате');
    }

    return this.messagesService.createMessage(userId, roomId, content);
  }

  @Get(':roomId')
  async getMessages(@Param('roomId') roomId: number, @Body('userId') userId: number) {
    const room = await this.roomsService.getRoomById(roomId, userId);
    if (!room) {
      throw new ForbiddenException('Нет доступа к комнате');
    }

    return this.messagesService.getMessagesByRoom(roomId);
  }
}
