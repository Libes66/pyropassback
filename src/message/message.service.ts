import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import {PrismaService} from "../prisma.service";

@Injectable()
export class MessageService {
  constructor(private prisma: PrismaService) {}

  async createMessage(userId: number, roomId: number, content: string) {
    return this.prisma.message.create({
      data: { userId,roomId,content},
    });
  }

  async getMessagesByRoom(roomId: number) {
    return this.prisma.message.findMany({
      where: { roomId },
      include: { user: true },
    });
  }
}
