import {Prisma} from "@prisma/client";

export const returnUserObject:Prisma.UserSelect ={
    id: true,
    username: true,
    email: true,
    image: true,
    password: false,
    createdAt:true,
    updatedAt:true,
}
