import {
    Controller,
    Get,
    Body,
    UsePipes,
    ValidationPipe,
    HttpCode,
    Put
} from '@nestjs/common';
import { UserService } from './user.service';
import {Auth} from "../auth/decorators/auth.decorator";
import {CurrentUser} from "../auth/decorators/user.decorator";
import {UserDto} from "./dto/user.dto";

@Controller('users')
export class UserController {

  constructor(private readonly userService: UserService) {}

  @Get('profile')
  @Auth()
   async getProfile(@CurrentUser('id') id:number) {
    return this.userService.byId(id)
  }

    @UsePipes(new ValidationPipe())
    @HttpCode(200)
    @Auth()
    @Put('profile')
    async updateProfile(@CurrentUser('id') id:number,@Body() Dto: UserDto) {
        return this.userService.updateProfile(id,Dto);
    }

    @Get('test')
    async getTest() {
      console.log('log')
        return {
            name:'teestMax241',
            age:20
        }
    }
}
