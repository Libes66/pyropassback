// src/s3/s3.service.ts
import { Injectable } from '@nestjs/common';
import { s3 } from '../config/aws.config';
import { v4 as uuid } from 'uuid';

@Injectable()
export class S3Service {
  private bucketName = process.env.S3_BUCKET_NAME;

  async uploadFile(file: Express.Multer.File, folder: string = ''): Promise<string> {
    const key = `${folder}/${uuid()}-${file.originalname}`;
    const params = {
      Bucket: this.bucketName,
      Key: key,
      Body: file.buffer,
      ContentType: file.mimetype,
    };

    await s3.upload(params).promise();
    return key;
  }

  async getFile(key: string): Promise<Buffer> {
    const params = {
      Bucket: this.bucketName,
      Key: key,
    };

    const data = await s3.getObject(params).promise();
    return data.Body as Buffer;
  }

  async listFiles(prefix: string = ''): Promise<string[]> {
    const params = {
      Bucket: this.bucketName,
      Prefix: prefix,
    };

    const data = await s3.listObjectsV2(params).promise();
    return data.Contents.map(item => item.Key);
  }

  async listFolders(prefix: string = ''): Promise<string[]> {

    const params = {
      Bucket: this.bucketName,
      // Prefix: prefix,
      Delimiter: '/',
    };

    const data = await s3.listObjectsV2(params).promise();
    return data.CommonPrefixes.map(item => item.Prefix);
  }

  async createFolder(folderName: string): Promise<void> {
    const key = `${folderName}/`;
    const params = {
      Bucket: this.bucketName,
      Key: key,
    };

    await s3.putObject(params).promise();
  }
}
