import { Controller, Post, UploadedFile, UseInterceptors, Get, Param, Query, Res, HttpException, HttpStatus, Body } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { S3Service } from './s3.service';
import { Response } from 'express';

@Controller('files')
export class S3Controller {
  constructor(private readonly s3Service: S3Service) {}
  @Get('folders')
  async listFolders(@Query('folder') folder?: string) {
    const folders = await this.s3Service.listFolders();
    return folders;
  }
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Query('folder') folder?: string) {
    if (!file) {
      throw new HttpException('File is required', HttpStatus.BAD_REQUEST);
    }

    const key = await this.s3Service.uploadFile(file, folder);
    return { key };
  }

  @Get(':key')
  async getFile(@Param('key') key: string, @Res() res: Response) {
    try {
      const file = await this.s3Service.getFile(key);
      res.setHeader('Content-Disposition', `attachment; filename=${key}`);
      res.send(file);
    } catch (error) {
      throw new HttpException('File not found', HttpStatus.NOT_FOUND);
    }
  }


  @Get()
  async listFiles(@Query('folder') folder?: string) {
    const files = await this.s3Service.listFiles(folder);
    return files;
  }

  @Post('create-folder')
  async createFolder(@Body('folderName') folderName: string) {
    if (!folderName) {
      throw new HttpException('Folder name is required', HttpStatus.BAD_REQUEST);
    }
    await this.s3Service.createFolder(folderName);
    return { message: 'Folder created successfully' };
  }
}
