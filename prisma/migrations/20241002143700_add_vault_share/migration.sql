/*
  Warnings:

  - You are about to drop the `VaultShare` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `caategory` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "VaultShare" DROP CONSTRAINT "VaultShare_userId_fkey";

-- DropForeignKey
ALTER TABLE "VaultShare" DROP CONSTRAINT "VaultShare_vaultId_fkey";

-- DropForeignKey
ALTER TABLE "caategory" DROP CONSTRAINT "caategory_sectionId_fkey";

-- DropForeignKey
ALTER TABLE "caategory" DROP CONSTRAINT "caategory_userId_fkey";

-- DropTable
DROP TABLE "VaultShare";

-- DropTable
DROP TABLE "caategory";

-- CreateTable
CREATE TABLE "category" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "code" TEXT NOT NULL,
    "image" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3),
    "createdBy" INTEGER NOT NULL,
    "updatedBy" INTEGER NOT NULL,
    "sectionId" INTEGER NOT NULL,
    "userId" INTEGER,

    CONSTRAINT "category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "vault_share" (
    "id" SERIAL NOT NULL,
    "vaultId" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "vault_share_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "category_code_key" ON "category"("code");

-- AddForeignKey
ALTER TABLE "category" ADD CONSTRAINT "category_sectionId_fkey" FOREIGN KEY ("sectionId") REFERENCES "section"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "category" ADD CONSTRAINT "category_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vault_share" ADD CONSTRAINT "vault_share_vaultId_fkey" FOREIGN KEY ("vaultId") REFERENCES "vault"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vault_share" ADD CONSTRAINT "vault_share_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
