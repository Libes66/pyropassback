-- CreateTable
CREATE TABLE "vault_share" (
    "id" SERIAL NOT NULL,
    "vaultId" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "vaultItemId" INTEGER,

    CONSTRAINT "vault_share_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "vault_share" ADD CONSTRAINT "vault_share_vaultId_fkey" FOREIGN KEY ("vaultId") REFERENCES "vault"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "vault_share" ADD CONSTRAINT "vault_share_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
