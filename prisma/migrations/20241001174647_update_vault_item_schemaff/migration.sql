/*
  Warnings:

  - You are about to drop the `vault_share` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "vault_share" DROP CONSTRAINT "vault_share_userId_fkey";

-- DropForeignKey
ALTER TABLE "vault_share" DROP CONSTRAINT "vault_share_vaultId_fkey";

-- DropTable
DROP TABLE "vault_share";

-- CreateTable
CREATE TABLE "VaultShare" (
    "id" SERIAL NOT NULL,
    "vaultId" INTEGER NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "VaultShare_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "VaultShare" ADD CONSTRAINT "VaultShare_vaultId_fkey" FOREIGN KEY ("vaultId") REFERENCES "vault"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "VaultShare" ADD CONSTRAINT "VaultShare_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
