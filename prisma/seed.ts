import { PrismaClient, VaultItemType } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
    // Создание пользователей
    const users = await Promise.all(
        Array.from({ length: 10 }, async (_, index) => {
            return prisma.user.create({
                data: {
                    email: `user${index + 1}@example.com`,
                    username: `user${index + 1}`,
                    password: `user${index + 1}`,
                    image: null,
                    status: index % 2 === 0 ? 'APPROVED' : 'PENDING',
                },
            });
        })
    );

    console.log('Пользователи созданы.');

    // Создание секций
    const sections = await Promise.all(
        Array.from({ length: 10 }, (_, index) =>
            prisma.section.create({
                data: {
                    name: `Section ${index + 1}`,
                    description: `Description for Section ${index + 1}`,
                    code: `SEC00${index + 1}`,
                    createdBy: users[index].id,
                    updatedBy: users[index].id,
                    userId: users[index].id, // Связь с пользователем (опционально)
                },
            })
        )
    );

    console.log('Секции созданы.');

    // Создание категорий
    const categories = await Promise.all(
        Array.from({ length: 10 }, (_, index) =>
            prisma.category.create({
                data: {
                    name: `Category ${index + 1}`,
                    description: `Description for Category ${index + 1}`,
                    code: `CAT00${index + 1}`,
                    image: null,
                    sectionId: sections[index].id,
                    createdBy: users[index].id,
                    updatedBy: users[index].id,
                    userId: users[index].id, // Связь с пользователем (опционально)
                },
            })
        )
    );

    console.log('Категории созданы.');

    // Создание vault
    const vaults = await Promise.all(
        Array.from({ length: 10 }, (_, index) =>
            prisma.vault.create({
                data: {
                    name: `Vault ${index + 1}`,
                    description: `Description for Vault ${index + 1}`,
                    code: `VAULT00${index + 1}`,
                    image: null,
                    createdBy: users[index].id,
                    updatedBy: users[index].id,
                },
            })
        )
    );

    console.log('Vaults созданы.');

    // Создание элементов в vault
    const vaultItems = await Promise.all(
        Array.from({ length: 10 }, (_, index) =>
            prisma.vaultItem.create({
                data: {
                    name: `VaultItem ${index + 1}`,
                    description: `Description for VaultItem ${index + 1}`,
                    code: `ITEM00${index + 1}`,
                    data: `Data for VaultItem ${index + 1}`,
                    type: getRandomVaultItemType(),
                    vaultId: vaults[index].id,
                },
            })
        )
    );

    console.log('VaultItems созданы.');

    // Создание расшаренных записей VaultShare
    const vaultShares = await Promise.all(
        Array.from({ length: 10 }, async (_, index) => {
            const vault = vaults[index];
            // Выбираем пользователя для расшаривания, отличный от создателя vault
            const sharedUser = users[(index + 1) % users.length];
            return prisma.vaultShare.create({
                data: {
                    vaultId: vault.id,
                    userId: sharedUser.id,
                },
            });
        })
    );

    console.log('VaultShares созданы.');
}

// Функция для случайного выбора типа VaultItemType
function getRandomVaultItemType(): VaultItemType {
    const types: VaultItemType[] = [
        'PASSWORD',
        'NOTES',
        'URL',
        'EXPIRATION_DATE',
        'FILE_ATTACHMENT',
    ];
    return types[Math.floor(Math.random() * types.length)];
}

main()
    .then(async () => {
        console.log('Сидирование завершено успешно.');
        await prisma.$disconnect();
    })
    .catch(async (e) => {
        console.error('Ошибка при сидировании:', e);
        await prisma.$disconnect();
        process.exit(1);
    });
